provider "aws" {
  profile = "${var.aws_profile}"
  region = "${var.aws_region}"
}

resource "aws_eip" "ip" {
  instance = "${aws_instance.example.id}"
  
  provisioner "local-exec" {
    command = "echo ${aws_eip.ip.public_ip} >> ip_address.txt"
  }
}

# New resource for the S3 bucket our application will use.
resource "aws_s3_bucket" "example" {
  # NOTE: S3 bucket names must be unique across _all_ AWS accounts, so
  # this name must be changed before applying this example to avoid naming
  # conflicts.
  bucket = "terraform-example-bucket1"
  acl    = "private"
}

resource "aws_instance" "example" {
  tags {
		Name = "Production_example"
	   }
  key_name	= "vikas"
  ami           = "ami-1ee65166"
  instance_type = "t2.micro"

  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} >> ip_address.txt"
  }
  
  # Tells Terraform that this EC2 instance must be created only after the S3 bucket has been created.
  depends_on = ["aws_s3_bucket.example"]  
}

#Non-Dependent Resources
#resource "aws_instance" "another" {
#  ami           = "ami-1ee65166"
#  instance_type = "t2.micro"
#}
